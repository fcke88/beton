import * as passport from 'koa-passport';
import * as Router from 'koa-router';
import { Inject } from 'typescript-ioc';

import { toRouter } from '@utils/routes-helpers';

import { AdminRouter } from './AdminRouter';
import { ApiRouter } from './ApiRouter';
import { Bot } from './Bot';
import catchError from './middleware/catch-error';
import { PassportLocalRegister } from './PassportLocalRegister';
import { ADMIN, API, HOOKS } from './paths.config';

export class MainRouter extends Router {

    constructor(
        @Inject adminRouter: AdminRouter,
        @Inject apiRouter: ApiRouter,
        @Inject bot: Bot,
        @Inject _: PassportLocalRegister
    ) {
        super();

        const botRouter = toRouter(bot);

        this
            .use(
                catchError, //format error to json output
                passport.initialize(), // authentication
                passport.session()
            )
            .use(ADMIN, adminRouter.routes(), adminRouter.allowedMethods())
            .use(API, apiRouter.routes(), apiRouter.allowedMethods())
            .use(HOOKS, botRouter.routes(), botRouter.allowedMethods())
    }
}