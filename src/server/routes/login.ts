import { Context } from 'koa';
import * as passport from 'koa-passport';

import { get, post } from '@utils/routes-helpers';

export class LoginRoutes {

    @get({ name: 'login' })
    login(ctx: Context) {
        ctx.pug('login');
    }

    @post({ path: '/login' })
    loginPost(ctx: Context, next: () => Promise<any>) {
        return passport.authenticate('local', async (_, user, info) => {
            if (user) {
                await ctx.login(user);
                ctx.namedRedirect('root');
            } else {
                ctx.addFlash((info ? info.message : null) || 'Invalid login data');
                ctx.namedRedirect('login');
            }
        })(ctx, next);
    }
}