import { hash } from 'bcryptjs';
import { Context } from 'koa';
import { setInterval } from 'timers';
import { Repository } from 'typeorm';
import { Inject } from 'typescript-ioc';

import { Entity } from '@crud/types';
import { User } from '@entities/User';
import detectEmailService from '@utils/detect-email-service';
import rndString from '@utils/rnd-string';
import { get, post } from '@utils/routes-helpers';
import sendEmail from '@utils/send-email';
import trim from '@utils/trim';

import { DBConnection } from '../DBConnection';
import catchError from '../middleware/redirect-after-error';

export class ResetPassRoutes {
    private userRepository: Repository<User>;
    private resetPasswordTokens: { [token: string]: { id: number, time: number } } = Object.create(null);

    constructor(@Inject connection: DBConnection) {
        this.userRepository = connection.getRepository(User);

        const RESET_PASSWORD_TOKENS_CLEAR_INTERVAL = 60000 * 20;

        setInterval(() => {
            const NOW = Date.now();
            for (let token in this.resetPasswordTokens) {
                if (NOW - this.resetPasswordTokens[token].time > RESET_PASSWORD_TOKENS_CLEAR_INTERVAL) {
                    delete this.resetPasswordTokens[token];
                }
            }
        }, RESET_PASSWORD_TOKENS_CLEAR_INTERVAL).unref();
    }

    @get({ name: 'resetpass' })
    resetpass(ctx: Context) {
        ctx.session.canreset = null;
        ctx.pug('resetpass');
    }

    @post({ path: '/resetpass' }, catchError('resetpass'))
    async resetpassPost(ctx: Context) {
        ctx.session.canreset = null;
        let { email } = <any>ctx.request.body;
        email = trim(email);

        const user = await this.userRepository.findOne({ email });

        if (!user) {
            throw 'User not found';
        }

        const { id } = user;
        for (let t in this.resetPasswordTokens) {
            if (this.resetPasswordTokens[t].id == id) {
                delete this.resetPasswordTokens[t];
            }
        }
        const token = await rndString();
        this.resetPasswordTokens[token] = { id, time: Date.now() };

        await sendEmail({
            email,
            url: ctx.resolve('resetpass') + '/' + token
        });

        ctx.logout();
        ctx.pug('resetpass-sended', { email, service: detectEmailService(email) });
    }

    @get({ path: '/resetpass/:token' }, catchError('login'))
    async resetToken(ctx: Context) {
        ctx.session.canreset = null;
        let token = this.resetPasswordTokens[ctx.params.token];
        if (!token) {
            throw 'Invalid or expiried recovery url';
        }
        delete this.resetPasswordTokens[ctx.params.token];
        const { id } = token;

        const user = id && await this.userRepository.findOne(id);

        if (!user) {
            throw 'Пользователь был удален. Попробуйте другой аккаунт.';
        }

        ctx.logout();
        ctx.session.canreset = id;
        ctx.pug('newpass', {
            action: ctx.resolve('newpassword')
        });
    }

    @post({ name: 'newpassword', path: '/newpassword' }, catchError('login'))
    async newpassword(ctx: Context) {
        const id = ctx.session.canreset;
        ctx.session.canreset = null;
        const user = id && await this.userRepository.findOne(id);

        if (!user) {
            throw 'Пользователь был удален. Попробуйте другой аккаунт.';
        }

        let { password } = <any>ctx.request.body;
        password = trim(password);

        (<any>User as Entity).insertValidate({
            email: user.email,
            password
        });

        user.password = await hash(password, 10);
        await this.userRepository.save(user);
        await ctx.login(user);
        ctx.namedRedirect('root');
    }
}