import { hash } from 'bcryptjs';
import { Context } from 'koa';
import { Repository } from 'typeorm';
import { Inject } from 'typescript-ioc';

import { Entity } from '@crud/types';
import { Settings } from '@entities/Settings';
import { User } from '@entities/User';
import { get, post } from '@utils/routes-helpers';
import trim from '@utils/trim';

import { DBConnection } from '../DBConnection';
import { FlashLevel } from '../middleware/flash';
import catchError from '../middleware/redirect-after-error';

export class InstallRoutes {
    @Inject private settings: Settings;

    private userRepository: Repository<User>;
    private settingsRepository: Repository<Settings>;

    constructor(@Inject connection: DBConnection) {
        this.userRepository = connection.getRepository(User);
        this.settingsRepository = connection.getRepository(Settings);
    }

    @get({ name: 'install' })
    install(ctx: Context) {
        if (this.settings.installed) {
            if (ctx.isAuthenticated()) {
                ctx.namedRedirect('root');
            } else {
                ctx.addFlash('Already installed. Login please.', FlashLevel.WARNING);
                ctx.namedRedirect('login');
            }
        } else {
            ctx.pug('install');
        }
    }

    @post({ path: '/install' }, catchError('install'))
    async installPost(ctx: Context) {
        if (this.settings.installed) {
            ctx.namedRedirect('install');
        } else {
            let { email, password } = <any>ctx.request.body;
            email = trim(email);
            password = trim(password);

            (<any>User as Entity).insertValidate({ email, password });

            let user = await this.userRepository.findOne({ email });

            if (user) {
                throw 'Пользователь с таким email уже существует.';
            }

            user = new User();
            user.email = email;
            user.password = await hash(password, 10);
            await this.userRepository.insert(user);

            this.settings.installed = true;
            this.settings.updateHookUrl(ctx);
            await this.settingsRepository.save(this.settings);

            await ctx.login(user);
            ctx.namedRedirect('root');
        }
    }
}