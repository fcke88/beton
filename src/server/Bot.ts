import { Context } from 'koa';
import * as TelegramBot from 'node-telegram-bot-api';
import * as getRawBody from 'raw-body';
import { Connection } from 'typeorm';
import { Inject, Singleton } from 'typescript-ioc';

import { Forecast } from '@entities/Forecast';
import { Settings } from '@entities/Settings';
import { Subscription } from '@entities/Subscription';
import { BaseRequestOpts, promiseRequest } from '@utils/cancelable-request';
import rndString, { rndStringSync } from '@utils/rnd-string';
import { post } from '@utils/routes-helpers';

@Singleton
export class Bot {
    @Inject private settings: Settings;
    private bot: TelegramBot = null;
    private subs: Subscription[] = [];
    private lastWebHookToken: string = rndStringSync();

    static async validateToken(token: string): Promise<boolean> {
        if (token) {
            try {
                const { body } = await promiseRequest('GET', `https://api.telegram.org/bot${token}/getMe`);
                return JSON.parse(body).result.is_bot;
            } catch (_) { }
        }
        return false;
    }

    async updateSubs(connection: Connection) {
        this.subs = await connection.getRepository(Subscription).find();
    }

    get hasSubs() {
        return this.subs.some(s => s.names.length > 0);
    }

    containsSub(v: string) {
        return this.subs.some(s => s.names.indexOf(v) > -1);
    }

    async update(token: string, connection: Connection) {
        if (!this.hasSubs) {
            await this.updateSubs(connection);
        }
        if (token && token == this.token) {
            return true;
        }
        if (await Bot.validateToken(token)) {
            try {
                if (token != this.token) {
                    const { body } = await promiseRequest('GET', {
                        url: `https://api.telegram.org/bot${token}/setWebhook`,
                        form: {
                            url: this.settings.telegramHookUrl + '/' + await this.rotateWebHookToken(),
                            max_connections: 2,
                            allowed_updates: ['channel_post']
                        }
                    });

                    if (JSON.parse(body).result != true) {
                        throw body.toString();
                    }

                    this.bot = new TelegramBot(token, { request: <any>BaseRequestOpts });

                    const subscriptionsRepository = connection.getRepository(Subscription);

                    this.bot.on('channel_post', async (m: TelegramBot.Message) => {
                        try {
                            const s = new Subscription();
                            s.title = m.chat.title || '';
                            s.telegramChannel = m.chat.id;
                            s.names = [];
                            try {
                                await subscriptionsRepository.save(s, { reload: false });
                                //при повторном получении поста из канала, его вставка вызовет ошибку и сообщение не будет удалено, таким образом будут удалятся только технические сообщения для бота
                                if (!this.hasSubs) {
                                    await this.bot.deleteMessage(m.chat.id, m.message_id.toString());
                                }
                            } catch (_) { }//ignore none unique chat id insert
                        } catch (err) {
                            console.error(err);
                        }
                    });
                }
                return true;
            } catch (err) {
                console.error(err);
            }
        }
        return false;
    }

    @post({ path: '/process-telegram-messages/:token' })
    async processTelegramMessages(ctx: Context) {
        if (ctx.params.token === this.lastWebHookToken && this.bot) {
            const updates = JSON.parse(await getRawBody(ctx.req, { limit: 1024 * 1024 * 10, encoding: 'utf-8' }));
            this.bot.processUpdate(updates);
        }
        ctx.status = 200;
        ctx.body = '';
    }

    async rotateWebHookToken() {
        return this.lastWebHookToken = await rndString();
    }

    get token() {
        //@ts-ignore
        return (this.bot && this.bot.token) || '';
    }

    async send(forecast: Forecast) {
        const r: (TelegramBot.Message | Error)[] = await Promise.all(
            this.subs
                .filter(sub => sub.names.indexOf(forecast.subscription) > -1)
                .map(({ telegramChannel }) => this.bot.sendMessage(telegramChannel, forecast.info, {
                    parse_mode: 'Markdown',
                    disable_web_page_preview: true
                }))
        );

        const e = r.find(e => e instanceof Error);
        if (e) {
            throw e;
        }

        return r.length > 0;
    }
}