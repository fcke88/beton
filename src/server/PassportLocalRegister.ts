import { compare } from 'bcryptjs';
import * as passport from 'koa-passport';
import { Strategy } from 'passport-local';
import { Inject, Singleton } from 'typescript-ioc';

import { User } from '@entities/User';
import trim from '@utils/trim';

import { DBConnection } from './DBConnection';

@Singleton
export class PassportLocalRegister {
    constructor(@Inject connection: DBConnection) {

        const userRepository = connection.getRepository(User);

        passport.serializeUser((user: User, done: Function) => {
            done(null, user.id);
        });

        passport.deserializeUser((id: number, done) => {
            userRepository.findOne(id)
                .then(user => done(null, user))
                .catch(done);
        });

        passport.use(new Strategy({
            usernameField: 'email',
            passwordField: 'password'
        }, async (email: string, password: string, done: Function) => {
            try {
                email = trim(email);
                password = trim(password);
                const user = await userRepository.findOne({ email })
                if (await compare(password, user.password)) {
                    done(null, user);
                } else {
                    done(null, false);
                }
            } catch (err) {
                done(err);
            }
        }));
    }
}