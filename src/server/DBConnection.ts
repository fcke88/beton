import * as fg from 'fast-glob';
import { readFile, writeFile } from 'fs';
import { Connection, createConnection } from 'typeorm';
import { SqljsDriver } from 'typeorm/driver/sqljs/SqljsDriver';
import { Provided } from 'typescript-ioc';
import { promisify } from 'util';

import entities from '@entities';
import { createAsync } from '@ioc/async-singleton-provider-factory';
import migrations from '@migrations';
import subscribers from '@subscribers';
import { trottleAsync } from '@taraflex/debounce';
import dataPath from '@utils/data-path';

const targetSQLiteFile = dataPath(APP_NAME + '.sqlite3');
const readFileAsync = promisify(readFile);
const writeFileAsync = promisify(writeFile);
const SQL = require('sql.js');

const provider = createAsync(async (): Promise<Connection> => {
    if (SQL.init) {
        await SQL.init();
        SQL.init = null;
    }

    let database: Buffer = null;
    try {
        database = await readFileAsync(targetSQLiteFile);
        if (database.length < 4096) {
            throw null;
        }
    } catch (_) {
        if (!DEBUG) {
            try {
                const filename = (await fg.async(__dirname + '/' + APP_NAME + '**.sqlite3'))[0] as string;
                if (filename) {
                    database = await readFileAsync(filename);
                }
            } catch (_) { }
        }
    }

    if (database && database.length < 4096) {
        database = null
    }

    const connection = await createConnection({
        type: 'sqljs',
        database,
        autoSave: true,
        autoSaveCallback: trottleAsync(async data => {
            if (data instanceof SQL.Database) {
                await writeFileAsync(targetSQLiteFile, data.export());
            }
        }, 20 * 1000, console.error),
        synchronize: !database,
        logging: false,
        entities,
        migrations,
        subscribers
    });
    const driver = connection.driver as SqljsDriver;
    driver.constructor.prototype.export = function () {
        return this.databaseConnection;
    };
    connection.constructor.prototype.exportRaw = function (): Uint8Array {
        return (this.driver as SqljsDriver).databaseConnection.export();
    }
    for (let e of entities) {
        //@ts-ignore
        if (e.__db_provider_init__) {
            //@ts-ignore
            await e.__db_provider_init__(connection);
            //@ts-ignore
            delete e.__db_provider_init__;
        }
    }
    try {
        await connection.manager.query('VACUUM');
    } catch (err) {
        console.error(err);
    }
    return connection;
});

export const dbInit = provider.init;

@Provided(provider)
export abstract class DBConnection extends Connection {
    abstract exportRaw(): Uint8Array;
}