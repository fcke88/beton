import * as Router from 'koa-router';
import { compileTemplate } from 'pug';
import { Inject } from 'typescript-ioc';
import { promisify } from 'util';
import { gzip } from 'zlib';

import { Action, Entity } from '@crud/types';
import entities from '@entities';
import { Settings } from '@entities/Settings';
import md5 from '@utils/md5';
import remoteDataName from '@utils/remote-data-name';
import resolveUrl from '@utils/resolve-url';
import { applyRoutes } from '@utils/routes-helpers';
import rtti2vue from '@utils/rtti-to-vue-component';

import { DBConnection } from './DBConnection';
import checkAuth from './middleware/check-auth';
import flash from './middleware/flash';
import installed from './middleware/installed';
import noCache from './middleware/no-cache';
import pug from './middleware/pug';
import smartRedirect from './middleware/smart-redirect';
import { InstallRoutes } from './routes/install';
import { LoginRoutes } from './routes/login';
import { ResetPassRoutes } from './routes/resetpass';
import { SiteClient } from './SiteClient';

const componentsTemplate = require('@templates/component.mustache');
const toSource = require('tosource');
const helmet = require('koa-helmet');
const compress = require('koa-compress');
const bodyParser = require('koa-bodyparser');
const paramCase = require('param-case');
const pluralize = require('pluralize');
const timestamp = require('time-stamp');
const pageTemplate: compileTemplate = require('@templates/index.pug');
const enforceHttps = require('koa-sslify');
const gzipAsync = promisify(gzip);

export class AdminRouter extends Router {

    constructor(
        @Inject dbconnection: DBConnection,
        @Inject settings: Settings,
        @Inject installRoutes: InstallRoutes,
        @Inject loginRoutes: LoginRoutes,
        @Inject resetPassRoutes: ResetPassRoutes,
        @Inject siteClient: SiteClient
    ) {
        super();
        this
            .use(
                enforceHttps({ trustProtoHeader: true }), //redirect to https
                helmet({ referrerPolicy: true }),
                bodyParser(), //parse request body
                smartRedirect, //redirect to named route    
                flash, //flash messages
                pug() //render pug template
            )
            .get('logout', '/logout', ctx => {
                ctx.logout();
                ctx.namedRedirect('login');
            });
        applyRoutes(this, installRoutes);
        if (!settings.installed) {
            this.use(installed(settings));
        }
        applyRoutes(this, loginRoutes);
        applyRoutes(this, resetPassRoutes);

        const componentsCache = {};
        const lastModified = new Date();

        this.get('root', '/', checkAuth, async ctx => {
            if (!componentsCache[ctx.state.user.id]) {
                const components = [];
                for (let entity of entities) {
                    let { name, rtti, checkAccess } = (<any>entity as Entity);
                    name = pluralize(name);

                    const can = Object.create(null);
                    for (let action of ['GET', 'PUT', 'POST', 'DELETE']) {
                        try {
                            can[action] = checkAccess(action as Action, ctx.state.user);
                        } catch (_) { }
                    }
                    if (can.GET) {
                        can.CHANGE = can.PUT || can.POST;
                        const render = rtti2vue(rtti, can);
                        const remote = Object.values(rtti.props)
                            .filter(v => v.remote)
                            .map(v => ({
                                name: remoteDataName(v.remote),
                                url: ctx.resolve(v.remote)
                            }));
                        components.push({
                            name,
                            icon: JSON.stringify(rtti.displayInfo.icon),
                            render,
                            can: toSource(can, null, false),
                            rtti: toSource(rtti, null, false),
                            url: ctx.resolve(paramCase(name)),
                            remote
                        });
                    }
                }
                const menu: any[] = [
                    { icon: 'downArrow', url: ctx.resolve('backup'), name: 'Backup', download: true },
                    { icon: 'signOut', url: ctx.resolve('logout'), name: 'Logout' },
                ];
                const code = Buffer.from(pageTemplate({
                    appTitle: APP_TITLE,
                    favicon: 'favicon.ico',
                    styles: [DEBUG ? null : 'index.css'],
                    scripts: ['index.js'],
                    assetsUrl: resolveUrl(ctx),
                    inlineScript: componentsTemplate({ components, menu })
                }));
                const etag = `W/"${md5(code, true)}"`;
                componentsCache[ctx.state.user.id] = {
                    code: await gzipAsync(code),
                    etag
                };
            }

            const { etag, code } = componentsCache[ctx.state.user.id];

            if (ctx.request.headers['if-none-match'] == etag) {
                ctx.status = 304;
                ctx.etag = etag;
                ctx.body = null;
            } else {
                ctx.type = 'html';
                ctx.lastModified = lastModified;
                ctx.etag = etag;
                ctx.set('Content-Encoding', 'gzip');
                ctx.body = code;
            }
        });

        this.get('backup', '/backup', noCache, checkAuth, compress(), ctx => {
            ctx.attachment(APP_NAME + '.' + timestamp('YYYY.MM.DD[HH.mm.ss]') + '.sqlite3');
            ctx.body = Buffer.from(dbconnection.exportRaw().buffer as ArrayBuffer);
        });

        this.get('subs', '/subs', noCache, checkAuth, ctx => {
            ctx.type = 'json';
            ctx.body = JSON.stringify(siteClient.subs);
        });
    }
}