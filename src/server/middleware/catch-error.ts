import * as m from 'builtin-status-codes';
import { Context } from 'koa';

import isJsonable from '@utils/is-jsonable';
import err2str from '@utils/stringify-error';

export default async (ctx: Context, next: () => Promise<any>) => {
    try {
        await next();
    } catch (err) {
        if (Array.isArray(err)) {
            //@ts-ignore
            ctx.status = err.status || 400;
            ctx.type = 'json';
            ctx.body = JSON.stringify({ errors: err.map(e => isJsonable(e) ? e : err2str(e)) });
        } else {
            const mr = m[err];
            const status = mr ? +err : (err && err.status) || 500;
            err = mr || err;

            if (status >= 500) {
                console.error(err);
                ctx.throw(500);
            } else {
                ctx.status = status;
                ctx.type = 'json';
                try {
                    //need catch error if frozen/sealed object or empty
                    err.status = undefined;
                } catch (_) { }
                ctx.body = JSON.stringify({ errors: [isJsonable(err) ? err : err2str(err)] });
            }
        }
    }
}