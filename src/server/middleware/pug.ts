import { Context } from 'koa';
import { compileTemplate, LocalsObject } from 'pug';

import resolveUrl from '@utils/resolve-url';

declare module 'koa' {
    interface Context {
        pug: (file?: string, locals?: LocalsObject) => void;
    }
}

export default (assetsUrl?: string) => function (ctx: Context, next: () => Promise<any>) {
    ctx.pug = (file?: string, locals?: LocalsObject) => {

        ctx.type = 'html';
        const template: compileTemplate = require('@templates/' + file + '.pug');

        ctx.body = template({
            appTitle: APP_TITLE,
            title: APP_TITLE,
            assetsUrl: assetsUrl || (assetsUrl = resolveUrl(ctx)),
            favicon: 'favicon.ico',
            styles: ['common.css'],
            ...ctx.flash,
            ...locals,
        });
    };
    return next();
}