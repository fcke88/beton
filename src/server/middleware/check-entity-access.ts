import { Context } from 'koa';

import { Action, Entity } from '@crud/types';

export default ({ checkAccess }: Entity) => function (ctx: Context, next: () => Promise<any>) {
    checkAccess(ctx.request.method as Action, ctx.state.user);
    return next();
}