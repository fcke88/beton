import { Context } from 'koa';
import { decode } from 'msgpack-lite';
import * as getRawBody from 'raw-body';

import options from '@utils/msgpack-options';

export default async (ctx: Context, next: () => Promise<any>) => {
    const { method } = ctx.request
    if (method === 'POST' || method === 'PUT') {
        const body = await getRawBody(ctx.req, { limit: 1024 * 1024 * 10 });
        ctx.request.body = Object.assign((body && body.length > 0 ? decode(body, options) : null), ctx.request.query);
    }
    return next();
}