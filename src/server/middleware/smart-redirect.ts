import { Context } from 'koa';
import * as Router from 'koa-router';

import resolveUrl from '@utils/resolve-url';

declare module 'koa' {
    interface Context {
        router: Router;
        namedRedirect: (routName: string) => void;
        resolve: (routName: string) => string;
    }
}

const routsCache = Object.create(null);

export default (ctx: Context, next: () => Promise<any>) => {

    ctx.resolve = routeName => routsCache[routeName] || (routsCache[routeName] = resolveUrl(ctx, ctx.router.url(routeName, null)));

    ctx.namedRedirect = routName => ctx.redirect(ctx.resolve(routName));

    return next();
}