import { Context } from 'koa';
import { encode } from 'msgpack-lite';

import options from '@utils/msgpack-options';

declare module 'koa' {
    interface Context {
        msgpack: (data: any) => void;
    }
}

export default (ctx: Context, next: () => Promise<any>) => {
    ctx.msgpack = (data: any) => {
        ctx.type = 'application/x-msgpack';
        if (Array.isArray(data)) {
            ctx.body = data.length > 0 ? encode({
                fields: Object.keys(data[0]),
                values: data.map(v => Object.values(v))
            }, options) : null;
        } else {
            ctx.body = data != null ? encode(data, options) : null;
        }
    }
    return next();
}