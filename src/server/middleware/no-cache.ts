import { Context } from 'koa';

export default function (ctx: Context, next: () => Promise<any>) {
    ctx.set('Cache-Control', 'no-cache, max-age=0');
    return next();
}