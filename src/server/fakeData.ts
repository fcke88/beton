const faker = require('faker');
const randomCountry = require('random-country');

export default () => {
    return `
<div class="pick_item">
    <table>
        <tr>
            <td class="sport tte8644743 ten" title="<div><b>Теннис</b></div>"><b>T</b></td>
            <td class="location">
                <div class="starts">
                    <div class="tte8644743" title="<div class='date_hint'><table><tr><td colspan='2'>От ввода прогноза: &nbsp; <b>11 ч. 2 м. </b></td></tr><tr><td colspan='2'>До начала события: &nbsp; <b>4 ч. 32 м. </b><br /><br /></td></tr><tr><td>Введено:</td><td>${new Date().toUTCString()}</td></tr><tr><td>Событие:</td><td>${new Date(Date.now() + 36000).toString()}</td></tr><tr><td>Интервал:</td><td><b>15 ч. 35 мин.</b> от ввода до события</td></tr></table></div>"><span class="starts_icon_gray"><b>4 ч. 32 м. </b></span></div>
                </div>${randomCountry({ full: true })}. ${faker.company.companyName()}</td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="event head">Событие</td>
            <td class="outcome head">Прогноз</td>
            <td class="stake tte8644743 head" title="Ставка, в процентах от банкролла">Размер</td>
            <td class="odds tte8644743 head" title="Коэффициент">Коэф.</td>
            <td class="book tte8644743 head" title="Букмекерская контора или биржа ставок">Контора</td>
        </tr>
        <tr>
            <td class="event">
                <a href="http://www.betonsuccess.ru/sub/14277/vernyakbets.T.UT/picks/8644743/">
                    <div class="event_main">${faker.company.companyName()} - ${faker.company.companyName()}</div>
                </a>
            </td>
            <td class="outcome tte" title="С форой">${(~~(Math.random() * 100)) / 10}</td>
            <td class="stake">${(~~(Math.random() * 1000)) / 10}%</td>
            <td class="odds"><b>${(~~(Math.random() * 100)) / 10}</b></td>
            <!-- <td class="book tte8644743  img_book" title="<b>Pinnacle</b>" ><a target="_blank"  rel="nofollow" href="http://www.pinnacle.com/ru/?refer=xbetoneng&amp;aup=True"><img src="/i/books/pinnacle.png"></a></td> -->
            <td class="book"><a target="_blank" rel="nofollow" href="http://www.pinnacle.com/ru/?refer=xbetoneng&amp;aup=True"><img src="/i/books/pinnacle.png"></a></td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="comment">
                <div></div>
                <div class="comment_sub_name">
                    <div class="user_pick_archive"><a href="javascript:up_archive_pick('8644743','active')">[в архив]</a></div>vernyakbets.T.UT</div>
            </td>
        </tr>
    </table>
</div>
`
}