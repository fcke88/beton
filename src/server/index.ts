import * as Koa from 'koa';
import * as mount from 'koa-mount';
import * as session from 'koa-session';
import * as serve from 'koa-static-serve';
import { Container, Inject } from 'typescript-ioc';

import { ParserSettings } from '@entities/ParserSettings';
import { Settings } from '@entities/Settings';

import { Bot } from './Bot';
import { DBConnection, dbInit } from './DBConnection';
import { MainRouter } from './MainRouter';
import { ADMIN, STATIC_DIR } from './paths.config';
import { SiteClient } from './SiteClient';

if (!(DEBUG || typeof v8debug === 'object' || /--debug|--inspect/.test(process.execArgv.join(' ')))) {
    require('pretty-error').start();
}

class App {
    private app: Koa;

    get koaApp() {
        return this.app;
    }

    constructor(
        @Inject settings: Settings,
        @Inject router: MainRouter,
        @Inject bot: Bot,
        @Inject siteClient: SiteClient,
        @Inject parserSettings: ParserSettings,
        @Inject connection: DBConnection
    ) {
        if (settings.installed) {
            bot.update(parserSettings.telegramBotToken, connection).then(() => {
                return siteClient.update(parserSettings, connection);//требует чтобы бот был по возможности проинициализирован в этому моменту
            });
        }

        const app = this.app = new Koa();

        //load secret from persistent storage (required by koa-session)
        app.keys = [settings.secret];
        app
            // sessions
            .use(session({
                key: '_',
                maxAge: 86400000 * 365,
                renew: true,
                httpOnly: true,
                signed: true
            }, app))
            //routes
            .use(router.routes())
            .use(router.allowedMethods())
            //static assets
            .use(mount(ADMIN, serve(STATIC_DIR, {
                maxAge: 365 * 24 * 60 * 60,
                sMaxAge: 365 * 24 * 60 * 60,
                dotfiles: 'deny',
                denyQuerystring: true,
                etag: true,
                lastModified: true,
                extname: ['.html'],
            })))
    }
}

async function main() {
    try {
        await dbInit();

        const { koaApp } = Container.get(App);

        const port = (+process.env.PORT >> 0) || 8080;
        const ip = process.env.IP || '0.0.0.0';

        await new Promise(resolve => koaApp.listen(port, ip, resolve));

        console.log(`Server listening on http://${ip}:${port}/`);
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
}

main();