import * as Router from 'koa-router';
import { Inject } from 'typescript-ioc';

import { Entity } from '@crud/types';
import entities from '@entities';
import data2Model from '@utils/data-to-model';

import { DBConnection } from './DBConnection';
import bodyParseMsgpack from './middleware/body-parse-msgpack';
import checkEntityAccess from './middleware/check-entity-access';
import msgpack from './middleware/msgpack';
import noCache from './middleware/no-cache';

const paramCase = require('param-case');
const pluralize = require('pluralize');

export class ApiRouter extends Router {

    constructor(@Inject connection: DBConnection) {
        super();
        this.use(
            bodyParseMsgpack, /* parse post/put msgpack encoded data */
            msgpack /* serialize output to msgpack*/
        );
        for (let entity of entities) {
            const e = <any>entity as Entity;
            const repository = connection.getRepository(e);

            const access = checkEntityAccess(e);
            const name = paramCase(pluralize(e.name));

            const saveModel = async (ctx, model) => {
                data2Model(ctx, model);
                const o = await repository.save(model);
                const { props } = e.rtti;
                for (let p in props) {
                    const v = props[p];
                    if (!v.generated && !v.primary) {
                        delete o[p];
                    }
                }
                ctx.status = 201;
                ctx.msgpack(o);
            }

            this
                .get(name, '/' + name, noCache, access, async ctx => {
                    ctx.msgpack(await repository.find(ctx.params));
                })
                .get(`/${name}/:id`, noCache, access, async ctx => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) {
                        throw 404;
                    }
                    ctx.msgpack(model);
                })
                .post('/' + name, access, ctx => saveModel(ctx, new e()))
                .put(`/${name}/:id`, access, async ctx => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) {
                        throw 404;
                    }
                    await saveModel(ctx, model);
                })
                .delete(`/${name}/:id`, access, async ctx => {
                    await repository.delete(ctx.params.id);
                    ctx.body = null;
                });
        }
    }
}