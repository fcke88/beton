import * as cheerio from 'cheerio';
import { basename } from 'path';
import * as request from 'request';
import { Connection, Repository } from 'typeorm';
import { Inject, Singleton } from 'typescript-ioc';
import { resolve, URL } from 'url';
import { runInNewContext } from 'vm';

import { Forecast } from '@entities/Forecast';
import { ParserSettings } from '@entities/ParserSettings';
import * as client from '@utils/cancelable-request';
import { CancelationToken, CancelationTokenError } from '@utils/cancelation-token';
import { delay } from '@utils/delay';
import md5 from '@utils/md5';
import trim from '@utils/trim';

import { Bot } from './Bot';

const rnd = require('random-number');
const parseDomain = require('domain-name-parser');
const sentenceCase = require('sentence-case');

const escMdMap = {
    '*': '\\*',
    '#': '\\#',
    '[': '\\[',
    _: '\\_',
    '\\': '\\\\',
    '`': '\\`',
    '<': '&lt;',
    '>': '&gt;',
    '&': '&amp;'
};

function escMd(s) {
    return s && s.replace(/[\*\[\\_`#<>]/g, m => escMdMap[m]);
}

function TEXT(c: Cheerio) {
    return escMd(c.text().trim());
}

const months = {
    'января': 'jan',
    'февраля': 'feb',
    'марта': 'mar',
    'апреля': 'apr',
    'мая': 'may',
    'июня': 'jun',
    'июля': 'jul',
    'августа': 'aug',
    'сентября': 'sep',
    'октября': 'oct',
    'ноября': 'nov',
    'декабря': 'dec'
}

function parseDate(s: string) {
    return new Date(s.replace(/января|февраля|марта|апреля|мая|июня|июля|августа|сентября|октября|ноября|декабря/i, m => months[m]).replace(/[^\w:\+\s]+/g, '').replace(/\s+/g, ' '));
}

function sessid(jar: request.CookieJar): request.Cookie {
    return jar && jar.getCookies('https://www.betonsuccess.ru/').find(c => c.key == 'PHPSESSID');
}

function sportIcon(s: string): string {
    switch (s) {
        case 'Теннис':
            return '🎾 ';
        case 'Футбол':
            return '⚽️ ';
        case 'Волейбол':
            return '🏐 ';
        case 'Бейсбол':
            return '⚾️ ';
        case 'Баскетбол':
            return '🏀 ';
        case 'Хоккей':
            return '🏒 ';
        case 'Киберспорт':
            return '🎮 ';
    }
    return '';
}

const headers = {
    Host: 'www.betonsuccess.ru',
    Origin: 'https://www.betonsuccess.ru',
    Referer: 'https://www.betonsuccess.ru/'
}

async function auth(user_name: string, user_password: string, ct: CancelationToken): Promise<request.CookieJar> {
    if (user_name && user_password) {
        const jar = request.jar();
        try {
            const user_hash = (await client.getDom({ url: 'https://www.betonsuccess.ru/', jar }, ct))('#user_hash_id').val();
            const resp = await client.post({
                url: 'https://www.betonsuccess.ru/login.php',
                jar,
                followRedirect: false,
                followAllRedirects: false,
                form: {
                    user_name,
                    user_password,
                    user_hash
                },
                headers
            }, ct);
            if (resp.statusCode == 302 && resp.headers.location == headers.Referer) {
                return jar;
            }
        } catch (_) {
            _;
        }
    }
    return null;
}

function toTextArray($: CheerioStatic, c: Cheerio): string[] {
    //@ts-ignore
    return c.map((_, e) => $(e).text().trim()).toArray();
}

function toHtmlArray($: CheerioStatic, c: Cheerio): string[] {
    //@ts-ignore
    return c.map((_, e) => $(e).html().trim()).toArray();
}

function findS(c: Cheerio): string {
    return c.last()
        .find('.comment_sub_name')[0]
        .children.find(n => n.type == 'text').data.trim().split(/\s+/, 2)[0];
}

@Singleton
export class SiteClient {
    @Inject private bot: Bot;

    private readonly settings: ParserSettings = Object.create(null);
    private forecastRepositiry: Repository<Forecast>;
    public subs: string[] = [];
    private ct: CancelationToken = new CancelationToken(true);

    async update(settings: ParserSettings, connection: Connection) {
        this.forecastRepositiry = connection.getRepository(Forecast);
        if (!settings) {
            return false;
        }
        const jar = await auth(settings.betonsuccessLogin, settings.betonsuccessPassword, new CancelationToken());
        if (!sessid(jar)) {
            return false;
        }
        if (settings.betonsuccessLogin != this.login || settings.betonsuccessPassword != this.password || !this.started) {
            Object.assign(this.settings, settings);
            this.stop(); //не смотря на автоостановку в start лучше остановиться заранее, чтобы ошибка остановки, если такая будет присутствовать, всплыла в этом стеке и приостановила обновление настроек
            this.start(jar);
        } else {
            Object.assign(this.settings, settings);
        }
        return true;
    }

    tableToForecast(tables: Cheerio): Forecast {
        if (tables.length < 3) {
            return null;
        }
        const $ = cheerio.load(tables.first().find('div[title]').attr('title'), { decodeEntities: false });
        const timers = toTextArray($, $('td'));
        const [, enterTimeStr, , actionTimeStr] = timers.splice(timers.findIndex(v => <any>v == 'Введено:'));
        const enterTime = parseDate(<any>enterTimeStr);
        if (
            this.settings.maximumPublishInterval <= 0 ||
            (Date.now() - enterTime.getTime()) <= this.settings.maximumPublishInterval * 1000
        ) {
            const sport = TEXT(cheerio.load(tables.first().find('.sport').attr('title'), { decodeEntities: false })('b'));
            const loc = tables.first().find('.location');
            loc.find('.starts').remove();
            const subscription = findS(tables);
            const infoRow = tables.eq(1).find('tr').eq(1);
            const book = infoRow.find('.book > a');
            const bookLink = book.attr('href') || 'https://www.betonsuccess.ru/';
            const bookName = sentenceCase(trim(book.text()) || parseDomain(basename(book.find('img').attr('src') || new URL(bookLink).hostname)).sld);
            const comment = tables.last().find('.comment').children().first();
            const eventLink = infoRow.find('.event > a');
            eventLink.children('.event_main').after('<span> </span>');
            const f = new Forecast();
            f.info = `
${sportIcon(sport)}${escMd(sport)}
*${TEXT(loc)}* 
${escMd(actionTimeStr || '')}
\`Событие    \` [${TEXT(eventLink)}](${escMd(resolve('https://www.betonsuccess.ru/', eventLink.attr('href')))})
\`Прогноз    \` ${TEXT(infoRow.find('.outcome'))}
\`Ставка     \` ${TEXT(infoRow.find('.stake'))}
\`Коэффициент\` ${TEXT(infoRow.find('.odds'))}
\`Контора    \` [${escMd(bookName)}](${escMd(bookLink)})
\`Подписка   \` ${escMd(subscription)}
${TEXT(comment)}`.trim();
            f.added = enterTime;
            f.subscription = subscription;
            return f;
        }
        return null;
    }

    private async process(jar: request.CookieJar, ct: CancelationToken) {
        if (!sessid(jar)) {
            throw null;
        }

        const $ = await client.getDom({ url: 'https://www.betonsuccess.ru/user/picks_active/', jar }, ct);

        this.subs = toTextArray($, $('.capper_L').first().closest('.left_module').find('.capper_L'));

        if (!this.bot.hasSubs) {
            return;
        }

        const scripts = toHtmlArray($, $('script[language="JavaScript"]'));
        const codes = scripts.filter(s => s.startsWith('eval(function(p,a,c,k'));

        if (codes.length < 1) {
            throw `Can't find packed script on page`;
        }

        const handlers = (<any>$('.open_button')
            .map((_, e) => {
                const self = $(e);
                const s = findS(self.closest('.pick_item').children('table'));
                if (!s || this.bot.containsSub(s)) {
                    return self.attr('onclick');
                } else {
                    return null;
                }
            })
            .toArray() as string[])
            .filter(s => s)
            .concat(scripts.filter(s => s && s.startsWith('spp(')))
            .join(';');

        if (!handlers) {
            return;
        }

        //remove old forecasts
        if (this.settings.removeAfter > 0) {
            try {
                await this.forecastRepositiry
                    .createQueryBuilder()
                    .delete()
                    .where(`added < date('now','-${this.settings.removeAfter >> 0} day')`)
                    .execute();
            } catch (err) {
                console.error(err);
            }
        }

        const requests = [];
        const sandbox = {
            hex_md5: md5,
            JsHttpRequest: {
                query(path, form) {
                    requests.push({ path, form });
                }
            }
        }

        runInNewContext(codes.join(';') + `;
var op = op || spp;
var spp = spp || op;` + handlers, sandbox);

        const ids: string[] = [];
        try {
            for (let { form, path } of requests) {
                const { body } = await client.post({
                    url: `https://www.betonsuccess.ru${path}?JsHttpRequest=${Date.now()}-xml`,
                    jar,
                    form,
                    headers
                    //proxy: 'http://127.0.0.1:8888' //fiddler4 proxy for debug
                }, ct);
                const { js } = JSON.parse(body);
                const forecast = this.tableToForecast(cheerio.load(js[Object.keys(js)[0]], { decodeEntities: false })('table'));
                if (forecast) {
                    const diffTime = Date.now() - forecast.added.getTime();
                    const minPub = this.settings.minimumPublishInterval * 1000;
                    if (diffTime >= minPub) {
                        await this.saveForecast(forecast, true);
                    } else {
                        setTimeout(this.saveForecast, minPub - diffTime, forecast);
                    }
                }
                const id = Object.values(form).find(v => v == +v >> 0) as string;
                if (id) {
                    ids.push(id);
                }
            }
        } finally {
            await this.flush();
            if (ids.length > 0) {
                try {
                    await Promise.all(ids.map(v => client.get({
                        jar,
                        url: 'https://www.betonsuccess.ru/user/picks_active/do_archive/?' + v,
                        followRedirect: false,
                        followAllRedirects: false
                    }, ct)));
                } catch (_) { }
            }
        }
    }

    private async flush() {
        for (let forecast of (await this.forecastRepositiry.find({ published: false }))) {
            try {
                if (await this.bot.send(forecast)) {
                    forecast.published = true;
                    await this.forecastRepositiry.save(forecast);
                }
            } catch (err) {
                console.error(err);
            }
        }
    }

    private saveForecast = async (forecast: Forecast, dontFlush?: boolean) => {
        try {
            await this.forecastRepositiry.save(forecast);
        } catch (_) { }
        if (!dontFlush) {
            await this.flush();
        }
    }

    stop() {
        this.ct.cancel();
    }

    private newToken() {
        this.stop();
        return this.ct = new CancelationToken();
    }

    get started() {
        return !this.ct.canceled;
    }

    async startIfIdle() {
        if (this.bot.hasSubs || this.subs.length < 1) {
            if (!this.started) {
                this.start(await auth(this.login, this.password, this.newToken()));
            }
        } else {
            this.stop();
        }
    }

    private async start(jar: request.CookieJar) {
        const ct = this.newToken();
        for (; ;) {
            try {
                if (!this.bot.hasSubs && this.subs.length > 0) {
                    return ct.cancel();
                }
                //Передаем ct и jar отдельно, чтобы в случае вызода start до окончания работы предыдущего start, 
                //старый продолжил исполнение с протухшим jar
                await this.process(jar, ct);
                await this.randomDelay(ct);
            } catch (err) {
                if (err instanceof CancelationTokenError) {
                    return;
                } else {
                    err && console.error(err);
                }
                await this.randomDelay(ct);
                //relogin after error
                jar = await auth(this.login, this.password, ct);
            }
        }
    }

    private randomDelay(ct: CancelationToken) {
        return delay(rnd({
            min: this.settings.minimumUpdateInterval * 1000,
            max: this.settings.maximumUpdateInterval * 1000,
            integer: true
        }), ct);
    }

    get login() {
        return this.settings.betonsuccessLogin;
    }

    get password() {
        return this.settings.betonsuccessPassword;
    }
}