import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { Access, v } from '@crud';

@Access({ 'GET': 0, 'DELETE': 0, 'PUT': 0 }, { icon: 'list' })
@Entity()
export class Subscription {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ readonly: true })
    @Column({ default: '' })
    title: string;

    @v({ readonly: true, pattern: /^-?\d+$/ })
    @Column({ unique: true })
    telegramChannel: number;

    @v({ remote: 'subs', items: { type: 'string', empty: false } })
    @Column({ type: 'simple-array' })
    names: string[];
}