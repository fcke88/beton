import { Forecast } from '@entities/Forecast';
import { ParserSettings } from '@entities/ParserSettings';
import { Settings } from '@entities/Settings';
import { Subscription } from '@entities/Subscription';
//import { Test } from '@entities/Test';
import { User } from '@entities/User';

export default [
    Settings,
    ParserSettings,
    User,
    Subscription,
    Forecast,
    //Test
]