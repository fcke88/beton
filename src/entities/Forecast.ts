import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

import { Access } from '@crud';

@Access({ 'GET': 0, 'DELETE': 0 }, { icon: 'volumeMedium' })
@Entity()
export class Forecast {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    added: Date;

    @Column()
    subscription: string;

    @Index({ unique: true })
    @Column()
    info: string;

    @Index()
    @Column({ default: false })
    published: boolean
}