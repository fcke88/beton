import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { Access, v } from '@crud';
import { SingletonEntity } from '@ioc';

const $1DAY = 60 * 60 * 24;

@SingletonEntity
@Access({ 'GET': 0, 'PUT': 0 }, { single: true, icon: 'gear' })
@Entity()
export class ParserSettings {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ pattern: /^\d{9}:[\w-]{35}$/, description: 'Токен телеграм бота [ℹ️ справка](https://www.youtube.com/watch?v=W0f66uie9e8)' })
    @Column({ default: '' })
    telegramBotToken: string;

    @v({ empty: false })
    @Column({ default: '' })
    betonsuccessLogin: string;

    @v({ empty: false })
    @Column({ default: '' })
    betonsuccessPassword: string;

    @v({ max: $1DAY, positive: true, description: 'Минимальный интервал опроса сайта в секундах. Истинный интервал будет выбираться случайным образом между минимальным и максимальным для маскировки бота.' })
    @Column({ type: 'int', default: 15, unsigned: true })
    minimumUpdateInterval: number;

    @v({ max: $1DAY, positive: true, description: 'Максимальный интервал опроса сайта в секундах.' })
    @Column({ type: 'int', default: 20, unsigned: true })
    maximumUpdateInterval: number;

    @v({ description: 'Минимальное время в секундах после публикации прогноза, для перепоста. Если скачали раньше то ожидаем недостающее время перед перепостом. Сетевые задержки не учитываются - будьте осторожны.' })
    @Column({ type: 'int', default: 0, unsigned: true })
    minimumPublishInterval: number;

    @v({ description: 'Максимальное время в секундах после публикации прогноза, для перепоста. Если опоздали то игнорируем прогноз. Не зависимо от интервала прогноз будет перепосчен единожды. ***0*** - делать перепост всегда независимо от времени публикации.' })
    @Column({ type: 'int', default: 0, unsigned: true })
    maximumPublishInterval: number;

    @v({ description: 'Удалить события из базы старше ***х*** дней (но не из телеграм канала). ***0*** - не удалять.' })
    @Column({ type: 'int', default: 7, unsigned: true })
    removeAfter: number;
}