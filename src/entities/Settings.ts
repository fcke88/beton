import { Context } from 'koa';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { SingletonEntity } from '@ioc';
import resolveUrl from '@utils/resolve-url';
import { rndStringSync } from '@utils/rnd-string';

import { HOOKS } from '../server/paths.config';

@SingletonEntity
@Entity()
export class Settings {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: () => "'" + rndStringSync() + "'" })
    secret: string;

    @Column({ default: false })
    installed: boolean;

    @Column({ default: '' })
    telegramHookUrl: string;

    updateHookUrl(ctx: Context) {
        this.telegramHookUrl = resolveUrl(ctx, HOOKS + '/process-telegram-messages');
    }
}