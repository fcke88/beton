import 'reflect-metadata';

import { ColumnOptions, getMetadataArgsStorage } from 'typeorm';

import {
    Action, AdditionalProperties, DisplayInfo, RTTIItem, Validator, ValidatorType
} from '@crud/types';
import isValidJsVar from '@utils/is-valid-js-var';
import { compile } from '@utils/validator-helpers';

function nullPrune(o: RTTIItem) {
    for (let k in o) {
        if (o[k] == null) {
            delete o[k];
        }
    }
    return o;
}

function getFieldSchema(target: Function, options: ColumnOptions, propertyName: string): RTTIItem {
    const { nullable, unsigned, array, zerofill, primary } = options;
    let type: ValidatorType;
    try {
        type = <any>(options.type as Function).name.toLowerCase();
    } catch (_) {
        type = <any>options.type.toString();
    }

    let enumPairs: any[][] = undefined;
    let enumValues: any[] = undefined;

    const generated = getMetadataArgsStorage().generations.some(v => v.target === target && v.propertyName === propertyName);

    if (Array.isArray(options.enum)) {
        enumValues = options.enum;
        enumPairs = enumValues.map(v => [v, v]);
    } else if (options.enum) {
        const keys = new Set([...Object.keys(options.enum)]);
        while (keys.size > 0) {
            const { value } = keys.values().next();
            let k: string = value;
            let v = options.enum[k];
            if (
                !isValidJsVar(k) ||
                (options.enum[v] === k && isValidJsVar(v) && v.toUpperCase() == v && k.toUpperCase() != k) //v constant case and k not
            ) {
                keys.delete(k);
                continue;
            }

            if (!Array.isArray(enumPairs)) {
                enumPairs = [];
                enumValues = [];
            }

            keys.delete(v.toString());
            keys.delete(k);
            enumPairs.push([k, v]);
            enumValues.push(v);
        }
    }

    let integer = undefined;

    if (options.type == 'date') {
        type = 'string';
    } else if (type.indexOf('int') > -1) {
        type = 'number';
        integer = true;
    } else if (type.indexOf('date') > -1 || type.indexOf('time') > -1) {
        type = 'date';
    } else if (type.indexOf('string') > -1 || type.indexOf('char') > -1 || type.indexOf('clob') > -1 || type.indexOf('text') > -1) {
        type = 'string';
    } else if (type.indexOf('bool') > -1) {
        type = 'boolean';
    } else if (type.indexOf('blob') > -1 || type.indexOf('binary') > -1) {
        //todo uint8array
        throw 'Not Validator implemented';
    } else if (<any>type == 'simple-array' || type == 'array' || array) {
        type = 'array';
    } else if (<any>type == 'simple-json') {
        type = 'object';
    } else {
        type = 'number';
    }

    const isNumber = type == 'number';

    //@ts-ignore
    const vo = target.__validatorMap && target.__validatorMap[propertyName];

    const o: RTTIItem = {
        type: enumValues && type != 'array' ? 'enum' : type,
        enumPairs,
        enum: enumValues,
        values: enumValues,
        integer,
        min: isNumber && (unsigned || zerofill || (vo && vo.positive)) ? 0 : undefined,
        max: isNumber && vo && vo.negative ? 0 : undefined,
        optional: !generated && (nullable || (options.default != null)),
        default: options.default == null && zerofill && isNumber ? 0 : options.default,
        primary,
        generated
    }

    if (!vo) {
        return nullPrune(o);
    }
    if (vo.constructor == String) {
        o.type = vo;
        return nullPrune(o);
    }
    return nullPrune(Object.assign(o, vo));
}

function validate(o) {
    const r = this(o);
    if (r !== true) {
        throw r;
    }
}

export function Access(allowedFor?: { [_ in Action]?: number }, displayInfo?: DisplayInfo) {
    return (entity: { new(): any }) => {
        const vInsert = Object.create(null);
        const vUpdate = Object.create(null);

        for (let { propertyName, options, target } of getMetadataArgsStorage().columns) {
            if (target === entity && options) {
                const schema = getFieldSchema(target as Function, options, propertyName);
                if (!schema.generated) {
                    vInsert[propertyName] = schema;
                }
                vUpdate[propertyName] = schema;
            }
        }

        allowedFor = allowedFor || Object.create(null);
        if (allowedFor._ !== +allowedFor._) {
            allowedFor._ = -1;
        }

        Object.defineProperty(entity, 'checkAccess', {
            value(action: Action, user: { role: number }) {
                let t = allowedFor[action];
                if (t == null) {
                    t = allowedFor._;
                }
                if (!user || (t & user.role) !== t) {
                    throw 403;
                }
                return true;
            }
        });

        for (const _ in vInsert) {
            Object.defineProperty(entity, 'insertValidate', {
                value: validate.bind(compile(vInsert))
            });
            break;
        }

        displayInfo = Object.freeze(displayInfo || Object.create(null));

        for (const _ in vUpdate) {
            Object.defineProperty(entity, 'updateValidate', {
                value: validate.bind(compile(vUpdate))
            });
            break;
        }

        Object.defineProperty(entity, 'rtti', {
            value: Object.freeze({ props: Object.freeze(vUpdate), displayInfo })
        });

        //@ts-ignore
        delete entity.__validatorMap;

        return entity;
    }
}

export function v(validatorProps: Validator & AdditionalProperties) {
    return (target: { constructor: Function }, prop: string | symbol): void => {
        //@ts-ignore
        (target.constructor.__validatorMap || (target.constructor.__validatorMap = Object.create(null)))[prop] = Object.freeze(validatorProps);
    };
}