export type ValidatorType = 'number' | 'string' | 'boolean' | 'array' | 'object' | 'forbidden' | 'date' | 'enum' | 'any';

export type ArrayValidator = {
    type?: ValidatorType,
    /**
     *If true, the validator accepts empty array[].
     */
    empty?: boolean,
    /**
     *Minimum count of elements.
     */
    min?: number,
    /**
     *Maximum count of elements.
     */
    max?: number,
    /**
     *Fix count of elements.
     */
    length?: number,
    /**
     *The array must contains this element too.
     */
    contains?: any,
    /**
     *Every element must be an element of the enum array.
     */
    enum?: any[],
    items?: Validator | Validator[]
}

export type AnyValidator = {
    type?: ValidatorType
}

export type BooleanValidator = {
    type?: ValidatorType,
    /**
     *if true and the type is not Boolean, try to convert. 1, "true", "1", "on" will be true. 0, "false", "0", "off" will be false.
     */
    convert?: boolean
}

export type DateValidator = {
    type?: ValidatorType,
    /**
     *if true and the type is not Date, try to convert with new Date().
     */
    convert?: boolean
}

export type EnumValidator = {
    type?: ValidatorType,
    /**
     *The valid values.
     */
    values?: any[]
}

export type ForbiddenValidator = {
    type?: ValidatorType
}

export type NumberValidator = {
    type?: ValidatorType,
    /**
     *Minimum value.
     */
    min?: number,
    /**
     *Maximum value.
     */
    max?: number,
    /**
     *Fix value.
     */
    equal?: number,
    /**
     *Can't be equal with this value.
     */
    notEqual?: number,
    /**
     *The value must be a non - decimal value.
     */
    integer?: boolean,
    /**
     *The value must be larger than zero.
     */
    positive?: boolean,
    /**
     *The value must be less than zero.
     */
    negative?: boolean,
    /**
     *if true and the type is not Number, try to convert with parseFloat.
     */
    convert?: boolean
}

export type ObjectValidator = {
    type?: ValidatorType,
    props: { [property: string]: Validator | Validator[] }
}

export type StringValidator = {
    type?: ValidatorType,
    /**
     *If true, the validator accepts empty string "".
     */
    empty?: boolean,
    /**
     *Minimum length of value.
     */
    min?: number,
    /**
     *Maximum length of value.
     */
    max?: number,
    /**
     *Fix length of value.
     */
    length?: number,
    /**
     *Regex pattern.
     */
    pattern?: RegExp,
    /**
     *The value must contains this text.
     */
    contains?: string,
    /**
     *The value must be an element of the enum array.
     */
    enum?: any[]
}

export type Validator = string | EnumValidator | ArrayValidator | DateValidator | AnyValidator | ForbiddenValidator | BooleanValidator | NumberValidator | ObjectValidator | StringValidator;

export type AdditionalProperties = {
    description?: string,
    placeholder?: string,
    remote?: string,
    readonly?: boolean
}

export type DisplayInfo = {
    icon?: string,
    single?: boolean
}

export type Action = ('GET' | 'POST' | 'PUT' | 'DELETE' | '_');

export type RTTIItem = Validator & AdditionalProperties & {
    type: ValidatorType,
    enumPairs?: any[][],
    optional: boolean,
    default?: any,
    primary: boolean,
    generated: boolean
}

export type RTTI = {
    displayInfo: DisplayInfo,
    props: { [property: string]: RTTIItem }
}

export type Entity = Function & {
    new(),
    insertValidate?: (o: any) => void,
    updateValidate?: (o: any) => void,
    checkAccess: (action: Action, user: { role: number }) => true,
    rtti: RTTI
}