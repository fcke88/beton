import { EntitySubscriberInterface, EventSubscriber, UpdateEvent } from 'typeorm';
import { Inject } from 'typescript-ioc';

import { ParserSettings } from '@entities/ParserSettings';

import { Bot } from '../server/Bot';
import { SiteClient } from '../server/SiteClient';

@EventSubscriber()
export class ParserSettingsSubscriber implements EntitySubscriberInterface<ParserSettings> {

    @Inject private bot: Bot;
    @Inject private client: SiteClient;

    listenTo() {
        return ParserSettings;
    }

    async beforeUpdate({ entity, connection }: UpdateEvent<ParserSettings>) {
        if (!await this.client.update(entity, connection)) {
            throw [
                {
                    field: 'betonsuccessLogin',
                    message: 'Invalid betonsuccess Login or Password'
                },
                {
                    field: 'betonsuccessPassword',
                    message: 'Invalid betonsuccess Login or Password'
                }
            ];
        }
        if (!await this.bot.update(entity.telegramBotToken, connection)) {
            throw [{
                field: 'telegramBotToken',
                message: 'Invalid telegram token'
            }];
        }
    }

}