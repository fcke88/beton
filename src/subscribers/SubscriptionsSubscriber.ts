import {
    EntitySubscriberInterface, EventSubscriber, InsertEvent, RemoveEvent, UpdateEvent
} from 'typeorm';
import { Inject } from 'typescript-ioc';

import { Subscription } from '@entities/Subscription';

import { Bot } from '../server/Bot';
import { SiteClient } from '../server/SiteClient';

@EventSubscriber()
export class SubscriptionsSubscriber implements EntitySubscriberInterface<Subscription> {

    @Inject private bot: Bot;
    @Inject private client: SiteClient;

    listenTo() {
        return Subscription;
    }

    async afterUpdate({ connection }: UpdateEvent<Subscription>) {
        await this.bot.updateSubs(connection);
        this.client.startIfIdle();
    }

    async afterInsert({ connection }: InsertEvent<Subscription>) {
        await this.bot.updateSubs(connection);
        this.client.startIfIdle();
    }

    async afterRemove({ connection }: RemoveEvent<Subscription>) {
        await this.bot.updateSubs(connection);
        this.client.startIfIdle();
    }
}