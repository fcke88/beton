import { ParserSettingsSubscriber } from '@subscribers/ParserSettingsSubscriber';
import { SubscriptionsSubscriber } from '@subscribers/SubscriptionsSubscriber';
import { ValidateSubscriber } from '@subscribers/ValidateSubscriber';

export default [
    ValidateSubscriber,
    ParserSettingsSubscriber,
    SubscriptionsSubscriber
];