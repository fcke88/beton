import { Provider } from 'typescript-ioc';

export interface AsyncProvider<T> extends Provider {
    init(...args): Promise<T>;
}

export function createAsync<T>(initCb: (...args) => Promise<T>): AsyncProvider<T> {
    let instance: T = null;
    return {
        async init(...args) {
            return instance = await initCb(...args);
        },
        get() {
            return instance;
        }
    }
}

export function create<T>(initCb: () => T): Provider {
    let instance: T;
    return {
        get() {
            return instance || (instance = initCb());
        }
    }
}