import { Connection } from 'typeorm';
import { Provided } from 'typescript-ioc';

import { createAsync } from '@ioc/async-singleton-provider-factory';

export function SingletonEntity(target: { new(): any }) {
    const provider = createAsync(async (connection: Connection) => {
        const repository = connection.getRepository(target);
        let instance = await repository.findOne();
        if (!instance) {
            await repository.insert(instance = new target());
        }
        return instance;
    });
    //@ts-ignore
    target.__db_provider_init__ = provider.init;
    Provided(provider)(target);
}