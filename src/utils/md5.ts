import { createHash } from 'crypto';

const encode32 = require('base32-encode');

export default function (s: string | Buffer | NodeJS.TypedArray | DataView, base32?: boolean): string {
    const h = createHash('md5').update(s);
    return base32 ? encode32(h.digest().buffer, 'Crockford') : h.digest('hex');
}