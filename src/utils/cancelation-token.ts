const PolyfilledSet = BROWSER ? require('es6-set') : Set;

type Connections = Set<CancelationTokenConnection>

export class CancelationTokenConnection {
    private cb: Function;
    private connections: Connections;
    constructor(connections: Connections, cb: Function) {
        connections.add(this);
        this.connections = connections;
        this.cb = cb;
    }
    run() {
        try {
            this.cb();
        } finally {
            this.dispose();
        }
    }
    dispose() {
        if (this.connections) {
            this.connections.delete(this);
            this.connections = null;
        }
    }
}

export class CancelationTokenError extends Error { }

export class CancelationToken {
    private connections: Connections = new PolyfilledSet();
    constructor(
        private _canceled = false
    ) { }
    get canceled() {
        return !!this._canceled;
    }
    check() {
        if (this._canceled) {
            throw new CancelationTokenError();
        }
    }
    connect(cb) {
        this.check();
        return new CancelationTokenConnection(this.connections, cb);
    }
    cancel() {
        if (!this._canceled) {
            this._canceled = true;
            let err = undefined;
            for (let c of this.connections) {
                try {
                    c.run();
                } catch (e) {
                    err = e || err;
                }
            }
            this.connections.clear();//в теории не обязательно - требуются тесты
            if (err !== undefined) {
                throw err;
            }
        }
    }
    /*Требуется доработка для обратотки ошибок
    async cancelAsync() {
        if (!this._canceled) {
            this._canceled = true;
            let err = undefined;
            for (let promise of Array.from(this._connections).map(c => c.cb())) {
                try {
                    await promise;
                } catch (e) {
                    err = e || err;
                }
            }
            this._connections.clear();
            if (err !== undefined) {
                throw err;
            }
        }
    }*/
}