import { CancelationToken, CancelationTokenError } from '@utils/cancelation-token';

function timeFn(connection, resolve) {
    connection.dispose();
    resolve(true);
}

export function delay(time: number, cancelationToken: CancelationToken): Promise<true> {
    return new Promise<true>((resolve, reject) => {
        const connection = cancelationToken.connect(() => {
            clearTimeout(timeout);
            reject(new CancelationTokenError());
        });
        const timeout = setTimeout(timeFn, time, connection, resolve);
    });
}

export function fastDelay(time: number) {
    return new Promise(resolve => { setTimeout(resolve, time); });
}