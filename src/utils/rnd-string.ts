import { randomBytes } from 'crypto';
import { promisify } from 'util';

const encode32 = require('base32-encode');
const rndBytes = promisify(randomBytes);

export default async (): Promise<string> => encode32((await rndBytes(~~(Math.random() * 7) + 23)).buffer as ArrayBuffer, 'Crockford').toLowerCase();

export function rndStringSync(): string {
    return encode32(randomBytes(~~(Math.random() * 7) + 23).buffer as ArrayBuffer, 'Crockford').toLowerCase();
}