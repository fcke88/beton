const tosource = require('tosource');

export default function stringify(o) {
    if (!o || o === !!o || o === +o) return String(o);
    if (o.constructor === String) return o;
    if (o.hasOwnProperty('toString') || typeof o === 'symbol') return o.toString();
    if (o.hasOwnProperty('toJSON')) return JSON.stringify(o);
    if (o instanceof Error) return o.name != 'Error' ? o.name + ': ' + o.message : o.message;
    return o.message ? stringify(o.message) : tosource(o);
}