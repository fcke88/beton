export default o => (
    !o ||
    o === !!o ||
    o === +o ||
    o.constructor === String ||
    o.constructor === Object ||
    o.hasOwnProperty('toJSON')
);