export default function (s?: string) {
    return s && s.constructor === String ? s.trim() : s;
}