import { Context } from 'koa';
import { resolve } from 'url';

import { ADMIN } from '../server/paths.config';

//todo check hsts enabled or ctx.secure or x-forward-proto before replace protocol
export default (ctx: Context, target?: string) => resolve(ctx.request.origin + '/', target || ADMIN + '/').replace(/^http:\/\//i, 'https://');