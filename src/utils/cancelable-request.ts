import * as cheerio from 'cheerio';
import * as request from 'request';

import { CancelationToken, CancelationTokenError } from '@utils/cancelation-token';
import decodeHtmlBuffer from '@utils/decode-html';

export type CookieJar = request.CookieJar;

export const BaseRequestOpts = Object.freeze({
    followRedirect: true,
    followAllRedirects: true,
    strictSSL: false,
    jar: false,
    timeout: 60 * 1000
});

const client = request.defaults({
    ...BaseRequestOpts,
    withCredentials: true,
    gzip: true,
    encoding: null,
    headers: {
        'Accept': '*/*',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'DNT': '1',
        'Pragma': 'no-cache',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
    }
});

function makeOpts(method: string, options: string | request.Options): request.Options {
    if (options.constructor === String) {
        return { method, url: options as string };
    } else {
        return Object.assign({ method }, options as request.Options);
    }
}

export async function getDom(options: request.Options, cancelationToken: CancelationToken) {
    const { body, headers } = await get(options, cancelationToken);
    return cheerio.load(decodeHtmlBuffer(headers['content-type'], <any>body), { decodeEntities: false });
}

export function http(method: string, options: string | request.Options, cancelationToken: CancelationToken): Promise<request.Response> {
    return new Promise((resolve, reject) => {
        const connection = cancelationToken.connect(() => {
            if (req) {
                req.removeAllListeners('end');
                req.removeAllListeners('response');
                req.abort();
            }
            req = null;
            reject(new CancelationTokenError());
        });

        let req: request.Request = client(makeOpts(method, options), (err, response: request.Response) => {
            connection.dispose();
            req = null;
            if (cancelationToken.canceled) {
                reject(new CancelationTokenError());
            } else if (err) {
                reject(err);
            } else if (response.statusCode >= 500) {
                reject({ code: response.statusCode, message: response.statusMessage });
            } else {
                resolve(response);
            }
        });
    });
}

export function promiseRequest(method: string, options: string | request.Options): Promise<request.Response> {
    return new Promise((resolve, reject) => {
        client(makeOpts(method, options), (err, response: request.Response) => {
            if (err) {
                reject(err);
            } else if (response.statusCode >= 500) {
                reject({ code: response.statusCode, message: response.statusMessage });
            } else {
                resolve(response);
            }
        });
    });
}

export function get(options: string | request.Options, cancelationToken: CancelationToken): Promise<request.Response> {
    return http('GET', options, cancelationToken);
}

export function post(options: string | request.Options, cancelationToken: CancelationToken): Promise<request.Response> {
    return http('POST', options, cancelationToken);
}