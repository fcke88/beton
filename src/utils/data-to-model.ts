import { Context } from 'koa';

import { Entity } from '@crud/types';

export default (ctx: Context, model: any) => {
    const { body } = ctx.request;
    const { props } = (model.constructor as Entity).rtti;
    for (let k in body) {
        const p = props[k];
        if (p && !p.generated) {
            model[k] = body[k];
        }
    }
}