'use strict';

const { basename, resolve } = require('path');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { DefinePlugin } = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const autoprefixer = require('autoprefixer');
const { VueLoaderPlugin } = require('vue-loader');
const sentenceCase = require('sentence-case');
const paramCase = require('param-case');
const { argv } = require('yargs');

function tsconfigPathsToAliases() {
    const { paths } = require('./tsconfig.json').compilerOptions;

    const aliases = {};

    Object.keys(paths).forEach((item) => {
        const key = item.replace('/*', '');
        const value = resolve('./', paths[item][0].replace('/*', ''));

        aliases[key] = value;
    });

    return aliases;
}

const DEV_SERVER = basename(require.main.filename) === 'webpack-dev-server.js';
const DEV = !!(DEV_SERVER || argv.watch);

const APP_TITLE = sentenceCase(require('./package.json').name);

const cssLoader = {
    loader: 'css-loader',
    options: {
        minimize: false,
        sourceMap: DEV
    }
}
const sassLoader = {
    loader: 'sass-loader',
    options: {
        sourceMap: DEV
    }
}
const postcssLoader = DEV ? undefined : {
    loader: 'postcss-loader',
    options: {
        plugins: [
            autoprefixer({
                browsers: ['last 2 version', '> 1%', 'ie >= 11']
            })
        ],
        sourceMap: DEV
    }
}

const extensions = ['.js', '.ts', '.jsx', '.tsx', '.vue'];

const u = (...a) => a.filter(e => !!e);

function config(browser) {
    const BROWSER_PROD = !DEV && browser;
    const styleLoader = DEV ? 'style-loader' : MiniCssExtractPlugin.loader;
    return {
        target: browser ? 'web' : 'node',
        devtool: DEV ? 'cheap-module-eval-source-map' : undefined,
        mode: DEV ? 'development' : 'production',
        entry: browser ? {
            index: './src/client/index.ts',
            common: './src/client/common.css',
        } : {
                index: './src/server/index.ts'
            },
        output: {
            devtoolModuleFilenameTemplate: '[absolute-resource-path]',
            path: resolve(__dirname, browser ? 'build/static' : 'build'),
            filename: '[name].js',
            chunkFilename: '[name].js'
        },
        resolve: {
            extensions,
            alias: {
                'mime-db$': resolve(__dirname, 'node_modules/mime-db/db.json'),
                'vue$': 'vue/dist/vue.runtime.esm.js',
                'cli-highlight$': resolve(__dirname, 'src/defines.ts'),
                'sql.js$': 'wasm-sql.js',
                'typescript-ioc$': 'typescript-ioc/es6',
                ...tsconfigPathsToAliases()
            },
        },
        node: browser ? {
            process: DEV,
            setImmediate: false,
        } : {
                __dirname: false
            },
        performance: { hints: false },
        optimization: {
            minimize: !DEV,
            minimizer: [new UglifyJSPlugin({
                uglifyOptions: {
                    keep_fnames: !browser,
                    compress: { collapse_vars: false },
                    output: { comments: false }
                }
            })]
        },
        plugins: u(
            new DefinePlugin({
                BROWSER: JSON.stringify(browser),
                DEBUG: JSON.stringify(DEV),
                APP_NAME: "'" + paramCase(APP_TITLE) + "'",
                APP_TITLE: "'" + APP_TITLE + "'",
                'process.env.ES6': true,//for typescript-ioc
                'process.env.NODE_ENV': JSON.stringify(DEV ? 'development' : 'production'),
                ...(browser ? { 'process.env.DEBUG': JSON.stringify(DEV) } : {})
            }),
            browser ? new VueLoaderPlugin() : 0,
            BROWSER_PROD ? new MiniCssExtractPlugin({ filename: '[name].css' }) : 0,
            BROWSER_PROD ? new OptimizeCssAssetsPlugin({
                cssProcessor: require('cssnano'),
                cssProcessorOptions: { discardComments: { removeAll: true }, zindex: false },
                canPrint: true
            }) : 0,
            browser ? new CopyWebpackPlugin([
                {
                    from: './src/client/favicon.ico',
                    to: './'
                }
            ]) : new CopyWebpackPlugin([
                {
                    from: './node_modules/wasm-sql.js/dist/sql-release-wasm.wasm',
                    to: './'
                }
            ]),
            DEV ? 0 : new BundleAnalyzerPlugin({
                analyzerMode: 'static',
                openAnalyzer: false,
                reportFilename: 'build-report.htm'
            })
        ),
        module: {
            exprContextCritical: false,
            rules: [{
                test: /\.scss$/,
                use: u(styleLoader, cssLoader, postcssLoader, sassLoader)
            }, {
                test: /\.css$/,
                use: u(styleLoader, cssLoader, postcssLoader)
            }, {
                test: /\.(t|j)sx?$/,
                loader: 'ts-loader',
                options: {
                    compilerOptions: {
                        sourceMap: DEV,
                        target: BROWSER_PROD ? 'es6' : 'ESNext',
                        noUnusedLocals: !DEV,
                        noUnusedParameters: !DEV
                    },
                    appendTsSuffixTo: [/\.vue$/]
                },
                exclude: /node_modules/
            }, {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        ts: 'ts-loader',
                        js: 'ts-loader'
                    },
                    esModule: true
                }
            }, {
                test: /\.(png|jpg|gif|webp)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 4096,
                            name: '[md5:hash:base58:10].[ext]',
                            outputPath: 'imgs/'
                        }
                    }
                ]
            }, {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8184,
                            name: '[md5:hash:base58:7].[ext]',
                            outputPath: 'fonts/'
                        }
                    }
                ]
            }, {
                test: /\.pug$/,
                loader: 'pug-loader',
                options: {
                    pretty: true
                }
            }, {
                test: /\.mustache/,
                loader: 'mustache-loader',
                options: {
                    tiny: true
                }
            }]
        },
        /* resolveLoader: {
             modules: ['node_modules', './src/loaders'],
             extensions: ['.js'],
             moduleExtensions: ['-loader']
             // mainFields: [ 'loader', 'main' ]
         },*/
        stats: {
            children: false,
            warningsFilter: /mongodb|mssql|mysql|mysql2|oracledb|pg|pg-native|pg-query-stream|redis|sqlite3|react-native/
        }
    }
}

switch (argv.target) {
    case 'web':
        module.exports = config(true);
        break;
    case 'node':
        module.exports = config(false);
        break;
    default:
        module.exports = [
            config(true),
            config(false),
        ];
        break;
}
